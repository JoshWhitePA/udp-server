#!/bin/python
#Author:Joshua White
#Professor:Lisa Frye
#Creation: November 4, 2015
#Due:November 9, 2015
#Filename:server.py
#Description:
# This program takes a character sent from a client and turns it into an upper case letter
# and sends it back to the client
import sys
import socket
import signal

port = 50009


try: #try for socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)#create socket
	s.bind(("", port))#binds, lol
except socket.error, e: #something bad happened
	print "Strange error creating socket: %s" % e
	sys.exit(1)
try: #catches ctrl - c to close sockets
	while 1:
		try: #in case there is a failure
			data, addr = s.recvfrom(1)#get one character from client
			if data != chr(10):#does not print out invisible line feed character
				print "Character from client: " + str(data)
				print "Chartacter transformed by the power of Josh: " + str(data).upper()
			s.sendto(str(data).upper(), addr) #need this to send line feed character to the client program
		except socket.error, e:
			print "Strange error receiving or sending from socket: %s" % e
			s.close()
			print "\nSocket Closed"
			sys.exit(1)
except KeyboardInterrupt:
    s.close()
    print "\nSocket Closed"
    sys.exit(1)
s.close()
